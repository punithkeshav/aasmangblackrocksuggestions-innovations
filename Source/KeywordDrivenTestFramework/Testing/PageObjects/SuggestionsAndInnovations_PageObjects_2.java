/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author syotsi
 */
public class SuggestionsAndInnovations_PageObjects_2 extends BaseClass {

    public static String supportingDocuments_Tabxpath() {
        return"//div[text()='Supporting Documents']";
    }
    //Navigate to Suggestions and Innovations

    public static String linkADoc_buttonxpath() {
        return"//div[text()='Upload Supporting Documents']//..//b[2]";
    }

    public static String urlInput_TextAreaxpath() {
        return"//input[@id='urlValue']";
    }

    public static String tile_TextAreaxpath() {
        return"//input[@id='urlTitle']";
    }

    public static String linkADoc_Add_buttonxpath() {
        return"//div[text()='Add']";
    }

    public static String saveSupportingDocs_buttonxpath() {
        return"//div[text()='Save supporting documents']";
    }

    public static String supportingDocument_xpath(String title) {
        return"//div[text()='" + title + "']";
    }

    public static String SaI_suggestionStatus(String data) {
        return"//a[text()='" + data + "']";
    }

    public static String SaI_suggestionStatus_DropDown() {
        return"//div[@id='control_93FFE3F6-8513-45B9-AE94-3C0E5E479A32']//span[2]";
    }

    public static String SaI_RejectedBy_DropDown() {
        return"//div[@id='control_7019AF67-B390-41B1-A44A-14D0253F5CA9']//li";
    }

    public static String SaI_RejectedBy(String data) {
        return"//ul[@class='select3-results jstree jstree-7 jstree-default jstree-loading']//a[text()='" + data + "']";
    }

    public static String SaI_dateRejected_textxpath() {
        return"//div[@id='control_0022993E-9324-4CF8-A7E7-8BD24FB332D1']//input";
    }

    public static String SaI_comments_textxpath() {
        return"//div[@id='control_A2A0F6C8-28B6-4607-B0B5-7DD5D243B521']//textarea";
    }

    public static String SaI_Save_button() {
        return"//div[@id='btnSave_form_F15BD897-46EC-4154-9F96-EC2C1FF039E9']//div[text()='Save']";
    }

    public static String SaI_savedStatus_xpath(String data) {
        return"//div[@id='control_93FFE3F6-8513-45B9-AE94-3C0E5E479A32']//li[text()='" + data + "']";
    }

}
