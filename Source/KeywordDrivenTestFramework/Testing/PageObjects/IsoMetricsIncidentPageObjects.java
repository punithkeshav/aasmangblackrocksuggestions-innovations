/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author MJivan
 */
public class IsoMetricsIncidentPageObjects 
{
 
    public static String environmentalHealthSafetyXpath()
    {
        return "//label[contains(text(),'Environmental, Health')]";
    }
    
    public static String allRecordID(){
        return "//td[@role='gridcell'][@class='no-wrap sortColumn']";
    }
    
    public static String environmentalHealth(){
        return "//div[@id='section_711036e0-fe8c-478f-a260-87beae7432fc']";
    }
    
    public static String seacrhButton(){
        return "//div[text()='Search']";
    }
    
    public static String IncidentManagmentXpath()
    {
        return "//label[contains(text(),'Incident Management')]";
    }

    public static String IncidentsManagment()
    {
         return "//label[contains(text(),'Incidents Management')]"; 
    }
    
    public static String ViewFilterBtn()
    {
        return "//div[@id='btnActFilter']";
    }
    
     public static String AddNewBtn()
    {
        return "//div[@id='btnActAddNew']";
    }
     
     public static String IncidentManagemetLoadingXpath(){
         return "//div[@id='divSearch']//div[@class='ui active inverted dimmer']";
     }
     
       public static String IncidentManagemetLoadingDoneXpath(){
         return "//div[@class='ui inverted dimmer hidden']//div[@title='Loading...']";
     }
     
     public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }
     
     public static String IncidentDescription()
     {
         return "//div[@id='control_E887FEB2-18C9-444E-A023-B05D5F08BC28']//textarea";
     }
     
     public static String IncidentOccured()
     {
         return "//div[@id='control_8FC01D3C-93D7-4570-BAEA-765533CDCB75']";
     }
     
     public static String Mining()
     {
         return "//a[text()='Mining ']//../i[@class='jstree-icon jstree-ocl']";
         //"a[@id='4cee9a75-7667-44e9-a0c1-77ad5092e86c_anchor']//../i[@class='jstree-icon jstree-ocl']";
     }
     
     public static String SouthAfrica()
     {
         return "//a[text()='South Africa']//../i[@class='jstree-icon jstree-ocl']";
     }
     
     public static String VictoryMine()
     {
         return "//a[text()='Victory Mine']//../i[@class='jstree-icon jstree-ocl']";
     }
     
     public static String Ore()
     {
         return "//a[text()='Ore Processing']";
     }
     
     public static String Panel()
     {
         return "//li[@id='4cee9a75-7667-44e9-a0c1-77ad5092e86c']";
     }
     
     public static String ProjectLink()
     {
         return "//div[@id='control_26DC542D-8D63-4388-B320-03B9C9ED7C88']//div[@class='c-chk']";
     }
     
     public static String Location()
     {
         return "//div[@id='control_0E9F8B1D-C36C-4999-9185-A5658E72A9DB']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
     }
     
     public static String PinToMap()
     {
         return "//div[@id='control_82B50C4C-22B2-46DB-8F8D-677EC2CA7D33']//div[@class='c-chk']";
     }
     
     
     public static String Project()
     {
         return "//div[@id='control_1D125078-AF69-492D-887E-0BE99AF4D528']";
     }
     
     public static String Simon()
     {
         return "//a[@id='91520b60-bfa6-4392-bb92-a9ebf8abcdd6_anchor']";
     }
     
     public static String SelectMap()
     {
         return "//span[text()='Map']";
     }
     
     public static String RiskDisciple()
     {
         return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']";
     }
     public static String impactTypeWait(){
         return "//a[contains(text(),'Business Risk')]";
     }
     public static String incedentTypeWait(){
         return "//a[contains(text(),'Business Loss')]";
     }
     public static String incidenttypeAll()
     {
         return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']//b[@original-title='Select all']";
     }
      public static String impacTypeAll()
     {
         return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']//b[@original-title='Select all']";
     }
     public static String Compliance()
     {
         return"//a[@id='b38c4a28-05f9-423d-a53e-3d2940d29f6f_anchor']//i[@class='jstree-icon jstree-checkbox']";
     }
     
     public static String SelectAll()
     {
         return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']//b[@class='select3-all']";
     }
     
     public static String IncidentClassification()
     {
         return "//div[@id='control_F6FDC1A4-AAE5-4C3E-B12B-EFD7A35FCB51']";
     }
     
     public static String Accident()
     {
         return"//a[@id='e11fcae9-3784-4677-8a89-5ea95443688b_anchor']//i[@class='jstree-icon jstree-checkbox']";
     }
     
     public static String Environmental()
     {
         return "//a[@id='dfa1dbcf-81f5-49b7-a6ba-a070cfbbff19_anchor']//i[@class='jstree-icon jstree-checkbox']";
     }
     
     public static String Injury()
     {
         return "//a[@id='75c54c6e-e0f2-49a1-bba3-575131962f3c_anchor']//i[@class='jstree-icon jstree-checkbox']";
     }
     
     public static String DatePicker()
     {
         return "//span[@class='k-widget k-datepicker k-header']";
     }
     
     public static String Date()
     {
         return "//a[@title='12 September 2018']";
     }
     
     public static String ClickUp()
     {
         return "//b[@class='select3-down drop_click']";
     }
     
     public static String IncidentTime()
     {
         return"//div[@id='control_2CE711B1-4A48-4381-916B-17F5FD255123']//input[@class='is-timeEntry']";
     }
     
     public static String Shift()
     {
         return "//div[@id='control_D47426BB-60CA-49A3-93BA-433B1E8BC4BD']";
     }
     
     public static String DayShift()
     {
         return "//a[text()='Day Shift']";
     }
     
     public static String ActionTaken()
     {
         return "//div[@id='control_58E573F1-1421-4B47-B9F9-68668A0AA36A']//textarea[@class='txt']";
     }
     
     public static String Reported()
     {
         return "//div[@id='control_0F48D804-ECD1-4700-B480-72338CD49E6F']";
     }
     
     public static String Save()
     {
         return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@i18n='save']";
     }
     
     public static String SuperVisor()
     {
         return "//div[@id='control_F2A12EB0-344C-47E6-BB85-B74B9B53CEFB']//ul[@class='select3-tagbox']";
     }
     
     public static String SelectAdmin()
     {
         return "//a[@id='b8ad5f90-d582-46c4-b186-d99649824acd_anchor']";
     }

    public static String ViewFilter_Button() 
    {
        return"//div[@id='btnActFilter']";
    }

    public static String ViewFilter__RecordNumber_textfield() 
    {
        return"//div[@id='ddlWhereType_7748DD6A-031D-48C0-8915-779360C3A7EF']//..//input[@class='txt border']";
    }

    public static String ViewFilter_Search_Button() 
    {
        return"//div[@id='btnActApplyFilter']";
    }
    
    public static String home(){
        return "//i[@class='icon health safety and environment link']";
    }
}
