/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author MJivan
 */
public class IncidentPageObjects {

    public static String environmentalHealthSafetyXpath() {
        return "//label[contains(text(),'Environmental, Health')]";
    }

    public static String highLightRecord(String id) {
        return "//span[text()='" + id + "']/../..";
    }

    public static String recordXpath(String id) {
        return "//span[text()='" + id + "']";
    }

    public static String getRecordIdXpath() {
        return "//div[@id='divPager'][@class='recnum']//div[@class='record']";
    }

    public static String getAssetRecord() {
        return "//div[@id='form_CEAD7953-CD9C-4E8A-ADB8-15C8AA6A092F']//div[@id='divPager'][@class='recnum']//div[@class='record']";
    }

    public static String clickClose() {
        return "//div[@id='form_B6196CB4-4610-463D-9D54-7B18E614025F']//i[@class='close icon cross']";
    }

    public static String incidentRefresh() {
        return "//a[@class='k-pager-refresh k-link']";
    }
    
    public static String riskRegisterform(){
        return "//div[@id='form_3AB45051-222E-416C-AAD3-86B2EDA98BED'][@class='form active transition visible']";
    }
    
    public static String tablexpath(){
        return "//table[@data-role]";
    }

    public static String dateOcXpath() {
        return "//div[@id='control_A68454D1-B0FB-4EB7-B861-2AF37ACAC8DF']//input";
    }

    public static String timeXpath() {
        return "//div[@id='control_2CE711B1-4A48-4381-916B-17F5FD255123']//input";
    }

    public static String reportedDateXpath() {
        return "//div[@id='control_991FEA22-9C36-4EC7-B385-744259EB6599']//input";
    }

    public static String reportedTimeXpath() {
        return "//div[@id='control_B623B7E6-DB94-4A72-933F-4847D48066D5']//input";
    }

    public static String shiftXpath() {
        return "//div[@id='control_D47426BB-60CA-49A3-93BA-433B1E8BC4BD']//li";
    }

    public static String partInvolvedXpath() {
        return "//div[@id='control_D086CF05-958D-4916-ADC3-BD45703467A5']/div[@class='c-chk']";
    }

    public static String occuredText() {
        return "//div[@id='control_8FC01D3C-93D7-4570-BAEA-765533CDCB75']//li";
    }

    public static String projectText() {
        return "//div[@id='control_8FC01D3C-93D7-4570-BAEA-765533CDCB75']//li";
    }

    public static String impactTypeText() {
        return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']//li";
    }

    public static String incidenttypeAll() {
        return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']//b[@original-title='Select all']";
    }

    public static String incidentText() {
        return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']//li";
    }

    public static String mapZoominPic() {
        return "ZoomIn.PNG";
    }

    public static String mapZoomOutPic() {
        return "ZoomOut.PNG";
    }

    public static String mapPic() {
        return "map1.PNG";
    }

    public static String imagePic1() {
        return "image1.PNG";
    }

    public static String fileNamePic1() 
    {
        return "FileName.PNG";
    }
    
     public static String fullReportPic1() {
        return "entertext.PNG";
    }


    public static String dvtPic() {
        return "dvt.PNG";
    }

    public static String dvt2Pic() {
        return "dvt2.PNG";
    }

    public static String dvt3Pic() {
        return "dvt3.PNG";
    }

    public static String image2Pic() {
        return "image2.PNG";
    }

    public static String image3Pic() {
        return "image3.PNG";
    }

    public static String openPic() {
        return "open.PNG";
    }
    
    public static String savePic() {
        return "Save.PNG";
    }


    public static String africaPic() {
        return "africa.PNG";
    }

    public static String joziPic() {
        return "jozi.PNG";
    }

    public static String joziPic2() {
        return "jozi2.PNG";
    }

    public static String markPic() {
        return "mark.PNG";
    }

    public static String markIconPic() {
        return "markIcon.PNG";
    }
    
    public static String downloadPic() {
        return "Downloading1.PNG";
    }
    
    public static String downloadPic2() {
        return "Downloading2.PNG";
    }

    public static String IncidentManagmentXpath() {
        return "//label[contains(text(),'Incident Management')]";
    }

    public static String IncidentsManagment() {
        return "//label[contains(text(),'Incidents Management')]";
    }

    public static String ViewFilterBtn() {
        return "//div[@id='btnActFilter']";
    }

    public static String AddNewBtn() {
        return "//div[@id='btnActAddNew']";
    }

    public static String iframeXpath() {
        return "//iframe[@id='ifrMain']";
    }

    public static String IncidentDescription() {
        return "//div[@id='control_E887FEB2-18C9-444E-A023-B05D5F08BC28']//textarea";
    }

    public static String IncidentOccured() {
        return "//div[@id='control_8FC01D3C-93D7-4570-BAEA-765533CDCB75']";
    }

    public static String globalCompanyXpath() {
        return "//a[text()='Global Company']//../i[@class='jstree-icon jstree-ocl']";
    }

    public static String clickGlobalCompanyXpath() {
        return "//a[text()='Global Company']";
    }

    public static String anyDropDownXpath(String option) {
        return "//a[text()='" + option + "']//../i[@class='jstree-icon jstree-ocl']";
    }

    public static String anyClickOptionXpath(String option) {
        return "//a[text()='" + option + "']";
    }

    public static String anyClickOptionCheckXpath(String option) {
        return "//a[text()='" + option + "']//../i[@class='jstree-icon jstree-checkbox']";
    }

    public static String southAfricaXpath() {
        return "//a[text()='South Africa']//../i[@class='jstree-icon jstree-ocl']";
    }

    public static String victorySiteXpath() {
        return "//a[text()='Victory Site']";
    }

    public static String specificLocationXpath() {
        return "//div[@id='control_0E9F8B1D-C36C-4999-9185-A5658E72A9DB']//input[@language]";
    }

    public static String Panel() {
        return "//li[@id='4cee9a75-7667-44e9-a0c1-77ad5092e86c']";
    }

    public static String ProjectLink() {
        return "//div[@id='control_26DC542D-8D63-4388-B320-03B9C9ED7C88']//div[@class='c-chk']";
    }

    public static String Location() {
        return "//div[@id='control_0E9F8B1D-C36C-4999-9185-A5658E72A9DB']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String PinToMap() {
        return "//div[@id='control_82B50C4C-22B2-46DB-8F8D-677EC2CA7D33']//div[@class='c-chk']";
    }

    public static String Project() {
        return "//div[@id='control_1D125078-AF69-492D-887E-0BE99AF4D528']";
    }

    public static String Simon() {
        return "//a[@id='91520b60-bfa6-4392-bb92-a9ebf8abcdd6_anchor']";
    }

    public static String SelectMap() {
        return "//span[text()='Map']";
    }

    public static String RiskDisciple() {
        return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']";
    }

    public static String Safety() {
        return "//a[@id='f86e0bdd-b06d-4122-a92a-2b8dcb4ba17d_anchor']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String incidentTypeSelectAllXpath() {
        return "//div[@id='control_F6FDC1A4-AAE5-4C3E-B12B-EFD7A35FCB51']//b[@class='select3-all']";
    }

    public static String incidentTypeTextXpath() {
        return "//div[@id='control_F6FDC1A4-AAE5-4C3E-B12B-EFD7A35FCB51']//b[@class='select3-all']";
    }

    public static String incidentTypeXpath() {
        return "//div[@id='control_F6FDC1A4-AAE5-4C3E-B12B-EFD7A35FCB51']//li";
    }

    public static String IncidentClassification() {
        return "//div[@id='control_F6FDC1A4-AAE5-4C3E-B12B-EFD7A35FCB51']";
    }

    public static String Accident() {
        return "//a[@id='e11fcae9-3784-4677-8a89-5ea95443688b_anchor']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String mapPicXpath() {
        return "//div[@id='control_6618F574-B672-4450-93CF-365F64F8CC02']";
    }

    public static String Environmental() {
        return "//a[@id='dfa1dbcf-81f5-49b7-a6ba-a070cfbbff19_anchor']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String Injury() {
        return "//a[@id='75c54c6e-e0f2-49a1-bba3-575131962f3c_anchor']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String DatePicker() {
        return "//span[@class='k-widget k-datepicker k-header']";
    }

    public static String Date() {
        return "//a[@title='12 September 2018']";
    }

    public static String ClickUp() {
        return "//b[@class='select3-down drop_click']";
    }

    public static String IncidentTime() {
        return "//div[@id='control_2CE711B1-4A48-4381-916B-17F5FD255123']//input[@class='is-timeEntry']";
    }

    public static String Shift() {
        return "//div[@id='control_D47426BB-60CA-49A3-93BA-433B1E8BC4BD']";
    }

    public static String DayShift() {
        return "//a[text()='Day Shift']";
    }

    public static String ActionTaken() {
        return "//div[@id='control_58E573F1-1421-4B47-B9F9-68668A0AA36A']//textarea[@class='txt']";
    }

    public static String Reported() {
        return "//div[@id='control_0F48D804-ECD1-4700-B480-72338CD49E6F']";
    }

    public static String sighOffActionXpath(String name) {
//        return "//div[contains(@class, 'transition visible')]//a[text()='" + name + "']";
        return "(//a[contains(text(),'"+name+"')])[2]";
    }
    
    public static String incidentOwner_select(String name){
        return "(//a[contains(text(),'"+name+"')])[1]";
    }

    public static String addMoreImagesRow1() {
        return "//div[@id='control_814FC712-ED13-4082-BD9B-CE2AE74F015B']";
    }

    public static String addMoreImagesRow2() {
        return "//div[@id='control_6A0ACBBC-54E4-423F-B8F3-5F6A0C3D1E89']";
    }

    public static String saveWait() {
        return "//div[@class='ui inverted dimmer active']";
    }
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    public static String submitWait(){
        return "//div[@class='ui inverted dimmer active']/div[text()='Saving...']";
    }
    public static String saveWait3() {
        return "//div[@class='ui inverted dimmer']";

    }
    
    public static String saveIncidents(){
        return "//div[@class='form transition visible active']";
    }

    public static String Save() {
        return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@i18n='save']";
    }

    public static String SuperVisor() {
        return "//div[@id='control_F2A12EB0-344C-47E6-BB85-B74B9B53CEFB']//ul[@class='select3-tagbox']";
    }

    public static String SelectAdmin() {
        return "//a[@id='b8ad5f90-d582-46c4-b186-d99649824acd_anchor']";
    }

    //New 
    public static String incidenttitleXpath() {
        return "//div[@id='control_E9C32F4B-AB3C-4ABB-B8E6-E5D5E34F139B']//input[@language]";
    }

    public static String markButtonXpath() {
        return "//div[@id='mapbox_point_b2ee88d5']";
    }

    public static String IncidentOwnerXpath() {
        return "//div[@id='control_4DC8AFD7-836E-4681-9FB8-99FAF564054C']//li";
    }

    public static String scrollXpath() {
        return "//div[@id='control_A5DA1B86-239A-4A5C-8C9C-873C38C0E605']";
    }

    public static String saveStep2() {
        return "//div[text()='Save and continue to Step 2']";
    }

    public static String submitXpath()
    {
        return "//div[text()='Submit 1.Event Report']";
    }

    public static String incident_Select(String text){
        return "(//a[contains(text(),'"+text+"')])[1]";
    }
    public static String loadingData() {
        return "//div[@class='ui inverted dimmer']/div[text()='Loading data...']";
    }
    public static String griedView_Mask(){
        return "(//div[@class='k-loading-mask'])[2]";
    }
    public static String griedView_Mask2(){
        return "(//div[@class='k-loading-mask']/div[@class='k-loading-image'])[2]";
    }
    
    public static String expand_global(){
        return "//a[text()='Global Company']/../i";
    }
    public static String expand_sa(){
        return "//a[text()='South Africa']/../i";
    }
    public static String expand_victory(){
        return "//a[text()='Victory Site']";
    }
    public static String external_part(String text){
        return "(//a[text()='"+text+"']/i[1])[1]";
    }
    public static String save(){
        return "(//div[text()='Save'])[2]";
    }
    
}
