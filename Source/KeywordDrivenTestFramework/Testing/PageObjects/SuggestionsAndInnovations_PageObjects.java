/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author syotsi
 */
public class SuggestionsAndInnovations_PageObjects extends BaseClass {
    //Navigate to Suggestions and Innovations
    public static String navigate_EHS() {
        return "//label[contains(text(),'Environmental, Health & Safety')]";
    }
    
    public static String suggestions_and_innovations(){
        return "//label[text()='Suggestions and Innovations']/..";
    }
    
    public static String SaI_Add(){
        return "//div[text()='Add']";
    }
    //Enter Details for Suggestions and Innovations
    public static String SaI_BusinessUnit(){
        return "//div[@id='control_5B861AF1-36B8-487E-98B0-103346048805']";
    }
    
    public static String SaI_BusinessUnit_Select(String text){
        return "//div[@id='divForms']/div[4]//a[text()='"+text+"']";
    }
    
    public static String SaI_ImpactType(){
        return "//div[@id='control_F9813D24-F97C-4B1E-A8D4-20DA5FE30F3D']/div/a/span[2]";
    }
    
    public static String SaI_ImpactType_SelectAll(){
        return "//div[@id='control_F9813D24-F97C-4B1E-A8D4-20DA5FE30F3D']//b[@original-title='Select all']";
    }
    
    public static String SaI_ImpactType_Select(String text){
        return "//div[@id='divForms']/div[5]//a[text()='"+text+"']/i[1]";
    }
    
    public static String SaI_Suggestion(){
        return "//div[@id='control_C3055DC1-068D-4EF7-9848-47C6CD32B294']//textarea";
    }
    
    public static String SaI_SuggestedBy(){
        return "//div[@id='control_0D470EFB-C6DB-445C-8E6C-5C7CF4C51AC7']/div/div/input";
    }
    
    public static String SaI_SuggestionAssignedTo(){
        return "//div[@id='control_9D45BA26-2B06-4424-9857-63C7331681C4']";
    }
    
    public static String SaI_SuggestionAssignedTo_Select(String text){
        return "//div[@id='divForms']/div[6]//a[contains(text(),'"+text+"')]";
    }
    
    public static String SaI_SuggestionLoggedBy(){
        return "//div[@id='control_83EB9154-C201-41B8-ACC4-93D7689D3AB9']";
    }
    
    public static String SaI_SuggestionLoggedBy_Select(String text){
        return "//div[@id='divForms']/div[7]//a[contains(text(),'"+text+"')]";
    }
    
    public static String SaI_SuggestionType(){
        return "//div[@id='control_31863877-ADC5-4799-8696-FDEBA2564DE5']";
    }
    
    public static String SaI_SuggestionType_Select(String text){
        return "//div[@id='divForms']/div[8]//a[contains(text(),'"+text+"')]";
    }
    //Save Buttons
    public static String SaI_SaveToContinue(){
        return "//div[@id='control_A770F207-6071-4BBC-95B0-31CA68442578']";
    }
    
    public static String SaI_Save_Button(){
        return "//div[@id='btnSave_form_F15BD897-46EC-4154-9F96-EC2C1FF039E9']";
    }
    
    public static String actionsTab(){
        return "//li[@id='tab_2394D3CF-1360-45E9-9E76-CB0BE35ED18A']";
//        return "//li[@id='tab_2394D3CF-1360-45E9-9E76-CB0BE35ED18A']//div[contains(text(),'Actions')]";
    }
    public static String actionPanel(){
        return "//div[@id='control_831E5C47-2208-4878-A346-B6B5E89E3A59']";
    }
    public static String addButton(){
        return "//div[@id='control_831E5C47-2208-4878-A346-B6B5E89E3A59']//div[contains(text(),'Add')]";
    }
    public static String actionDescriptionTextarea(){
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']";
    }
    public static String actionDescriptionTextareaField(){
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }
    public static String departmentResponsibleTab(){
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']";
    }
    public static String departmentResponsible(String text){
        return "//div[@id='divForms']//div[14]//a[contains(text(),'" + text + "')]";
    }
    public static String responsiblePersonTab(){
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']";
    }
    public static String responsiblePerson(String text){
        return "//div[@id='divForms']//div[15]//a[contains(text(),'" + text + "')]";
    }
    public static String actionDueDateTab(){ 
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']";
    }
    public static String actionDueDate(){ 
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }
    
    public static String replicateChkbx(){
        return "//div[@id='control_9FB64F38-240A-4D57-8EBF-202D8124CDEE']";
    }

    public static String multipleUsersTab(){
        return "//div[@id='control_B85CC3EF-7F60-4CE6-8F64-E978C40DD033']";
    }
    public static String selectMultipleUsers(){
        return "//div[@id='control_B85CC3EF-7F60-4CE6-8F64-E978C40DD033']//b[@class='select3-all']";
    }
    public static String saveButton(){
        return "//div[@id='btnSave_form_686409B9-6012-4299-9966-227865B6C302']//div[text()='Save']";
    }

    public static String SaI_SuggestionStatus(){
        return "//div[@id='control_93FFE3F6-8513-45B9-AE94-3C0E5E479A32']";
    }
    
    public static String SaI_SuggestionStatus_Select(String text){
        return "//div[@id='divForms']/div[9]//a[contains(text(),'"+text+"')]";
    }
    public static String suggestionPriorityTab(){
        return "//div[@id='control_712AB1A8-6744-4D27-8744-7C3B552B94CA']";
    }
    public static String suggestionPriority(String text){
        return "//div[@id='divForms']/div[10]//a[contains(text(),'" + text + "')]";
    }
    public static String approvedByTab(){
        return "//div[@id='control_80FAB30D-37BE-484D-BD06-91CF8304AE3A']";
    }
    public static String approvedBy(String text){
        return "//div[@id='divForms']/div[11]//a[contains(text(),'" + text + "')]";
    }
    public static String dateApprovedTab(){
        return "//div[@id='control_C52E6F32-0184-447B-8184-F466A90A5048']";
    }

    public static String dateApproved() {
        return "//div[@id='control_C52E6F32-0184-447B-8184-F466A90A5048']//input";
    }

    public static String commentsTextarea() {
       return "//div[@id='control_A2A0F6C8-28B6-4607-B0B5-7DD5D243B521']";
    }
    public static String commentsTextareaField() {
        return "//div[@id='control_A2A0F6C8-28B6-4607-B0B5-7DD5D243B521']//textarea";
    }
    
    public static String SaI_Upload(){
        return "//b[@class='linkbox-upload']";
    }
    public static String dvt2Pic(){
        return "dvt2.png";
    }
    public static String SaI_Search(){
        return "FileName.PNG";
    }
    public static String openPic(){
        return "open.PNG";
    }
    
    public static String save_wait(){
        return "//div[@class='processFlow']/..//div[text()='Record saved']";
    }
    public static String processFlow() {
        return "//div[@id='btnProcessFlow_form_F15BD897-46EC-4154-9F96-EC2C1FF039E9']";
    }
    public static String processFlow2() {
        return "//div[@id='btnProcessFlow_form_686409B9-6012-4299-9966-227865B6C302']";
    }
}
