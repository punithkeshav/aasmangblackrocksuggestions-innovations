/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.SuggestionsAndInnovations_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Suggestions and Innovation Alt4",
        createNewBrowserInstance = false
)

public class FR1_Capturing_Suggetions_and_Innovations_AlternateScenario4 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR1_Capturing_Suggetions_and_Innovations_AlternateScenario4()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
//        if (!IsometrixNavigateSuggestionsAndInnovations())
//        {
//            return narrator.testFailed("Navigation Failed due to - " + error);
//        }
        
//        if(!enterDetails()){
//            return narrator.testFailed("Enter Details Failed due to - " + error);
//        }
        
        if(!changeDetails()){
            return narrator.testFailed("Change Details Failed due to - " + error);
        }    
        
        return narrator.finalizeTest("Completed navigate to Suggestions And Innovations");
    }

    public boolean IsometrixNavigateSuggestionsAndInnovations(){
        //switch to the iframe
//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
//
//        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
//        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Risk Management
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.suggestions_and_innovations())){
            error = "Failed to wait for 'Suggestions And Innovations.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.suggestions_and_innovations())){
            error = "Failed to click on 'Suggestions And Innovations' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Suggestions And Innovations' tab.");
       
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_Add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_Add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }
    
    public boolean enterDetails(){
        //Business Unit
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_BusinessUnit())){
            error = "Failed to wait for 'Business Unit' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_BusinessUnit())){
            error = "Failed to click on 'Business Unit' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_BusinessUnit_Select(getData("Business Unit")))){
            error = "Failed to wait for 'Business Unit' option: '" + getData("Business Unit") + "'.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_BusinessUnit_Select(getData("Business Unit")))){
            error = "Failed to select 'Business Unit' option: '" + getData("Business Unit") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Business unit '" + getData("Business Unit") + "' has been entered.");
        pause(2000);
        
        //Impact Type
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_ImpactType())){
            error = "Failed to wait for 'Impact Type' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_ImpactType())){
            error = "Failed to click on 'Impact Type' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_ImpactType_SelectAll())){
            error = "Failed to wait for 'Impact Type' select all button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_ImpactType_SelectAll())){
            error = "Failed to click on 'Impact Type' select all button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Impact type selected.");
        
        //Suggestion
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_Suggestion())){
            error = "Failed to wait for 'Suggestion' textarea.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(SuggestionsAndInnovations_PageObjects.SaI_Suggestion(), getData("Suggestion"))){
            error = "Failed to enter text into 'Suggestions' textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Suggestion entered.");
        
        //Suggested By
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestedBy())){
            error = "Failed to wait for 'Suggested By' textarea.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestedBy(), getData("Suggested by"))){
            error = "Failed to enter text into 'Suggested By' textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Suggested by '" + getData("Suggested by") + "'.");
        
        //Suggestion Assigned to
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionAssignedTo())){
            error = "Failed to wait for 'Suggestion Assigned to' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionAssignedTo())){
            error = "Failed to click on 'Suggestion Assigned to' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionAssignedTo_Select(getData("Assigned to")))){
            error = "Failed to wait for '" + getData("Assigned to") + "' in Assigned To dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionAssignedTo_Select(testData.getData("Assigned to")))){
            error = "Failed to select '" + getData("Assigned to") + "' from Assigned To dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Assigned to '" + getData("Assigned to") + "' is selected.");
        
        //Logged By
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionLoggedBy())){
            error = "Failed to wait for 'Logged By' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionLoggedBy())){
            error = "Failed to click 'Logged by' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionLoggedBy_Select(getData("Logged by")))){
            error = "Failed to wait for '" + getData("Logged by") + "' in Logged by dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionLoggedBy_Select(testData.getData("Logged by")))){
            error = "Failed to click '" + getData("Logged by") + "' from Logged by dropdwon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Logged by '" + getData("Logged by") + "' is selected.");
        
        //Suggestion type
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionType())){
            error = "Failed to wait for 'Suggestion type' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionType())){
            error = "Failed to click on 'Suggestion type' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionType_Select(getData("Suggestion type")))){
            error = "Failed to wait for '" + getData("Suggestion type") + "' in Suggestion type dropwdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionType_Select(getData("Suggestion type")))){
            error = "Failed to click '" + getData("Suggestion type") + "' from Suggestion type dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Suggestion type '" + getData("Suggestion type") + "' is selected.");
        narrator.stepPassedWithScreenShot("Details entered.");
        
        //Save to continue button
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SaveToContinue())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SaveToContinue())){
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Save to continue' button.");
        
        return true;
    }
    
    public boolean changeDetails(){
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionStatus())){
            error = "AAAAAAAAHHHHHHHH";
            return false;
        }
        //Suggestion Status
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionStatus())){
            error = "Failed to wait for 'Suggestion Status' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionStatus())){
            error = "Failed to click on 'Suggestion Status' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionStatus_Select(getData("Suggestion Status")))){
            error = "Failed to wait for '" + getData("Suggestion Status") + "' in Suggestion Status dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionStatus_Select(getData("Suggestion Status")))){
            error = "Failed to select '" + getData("Suggestion Status") + "' in Suggestion Status dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Suggestion Status changed to '" + getData("Suggestion Status") + "' .");
        
        //Suggestion priority
//        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.suggestionPriorityTab())){
//            error = "Failed to wait for 'Suggestion priority' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.suggestionPriorityTab())){
//            error = "Failed to click on 'Suggestion priority' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.suggestionPriority(getData("Suggestion Priority")))){
//            error = "Failed to wait for '" + getData("Suggestion Priority") + "' option.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.suggestionPriority(getData("Suggestion Priority")))){
//            error = "Failed to click on '" + getData("Suggestion Priority") + "' option.";
//            return false;
//        }
//        pause(2000);
//        narrator.stepPassedWithScreenShot("Suggestion priority '" + getData("Suggestion Priority") + "' has been selected.");
//        
//        //Approved by
//        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.approvedByTab())){
//            error = "Failed to wait for 'Approved by' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.approvedByTab())){
//            error = "Failed to wait for 'Approved by' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.approvedBy(getData("Approved by")))){
//            error = "Failed to wait for '" + getData("Approved by") + "' option.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.approvedBy(getData("Approved by")))){
//            error = "Failed to click on '" + getData("Approved by") + "' option.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Approved by '" + getData("Approved by") + "' has been selected.");
//        
//        //Date Approved
//        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.dateApprovedTab())){
//             error = "Failed to wait for 'Date approved' tab";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(SuggestionsAndInnovations_PageObjects.dateApproved(), getData("Date approved"))){
//             error = "Failed to enter '" + getData("Date approved") + "' into 'Date approved' field";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Date approved '" + getData("Aproved by") + "' has been selected.");
//        
//        //Comments textarea
//        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.commentsTextarea())){
//            error = "Failed to wait for 'Comments' textarea";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(SuggestionsAndInnovations_PageObjects.commentsTextareaField(), getData("Comments"))){
//            error = "Failed to enter '" + getData("Comments") + "' into 'Comments' textarea";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully commented '" + getData("Comments") + "'.");
        
        //Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_Save_Button())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_Save_Button())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
        
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        
        return true;
    }

}
