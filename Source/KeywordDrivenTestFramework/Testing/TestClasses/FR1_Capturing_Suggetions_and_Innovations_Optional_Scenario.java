/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.IsoMetricsIncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.SuggestionsAndInnovations_PageObjects_2;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SYotsi
 */
@KeywordAnnotation(
        Keyword = "Capturing Suggestions and Innovation Optional Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capturing_Suggetions_and_Innovations_Optional_Scenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capturing_Suggetions_and_Innovations_Optional_Scenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!NavigateToTheSupportingDocumentsTab()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed Capturing Suggestions and Innovation - Optional Scenario.");
    }

    public boolean NavigateToTheSupportingDocumentsTab() {
        //Navigate to Environmental Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.supportingDocuments_Tabxpath())) {
            error = "Failed to wait for 'Supporting Documents' tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects_2.supportingDocuments_Tabxpath())) {
            if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.supportingDocuments_Tabxpath())) {
                error = "Failed to wait for 'Supporting Documents' tab.";
                return false;
            }
            pause(5000);
            if (!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects_2.supportingDocuments_Tabxpath())) {
                error = "Failed to click on 'Supporting Documents' tab.";
                return false;
            }
        }

        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Supporting Documents' tab.");

        //Upload a link
        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.linkADoc_buttonxpath())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects_2.linkADoc_buttonxpath())) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassed("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow()) {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.urlInput_TextAreaxpath())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(SuggestionsAndInnovations_PageObjects_2.urlInput_TextAreaxpath(), testData.getData("Document url"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.tile_TextAreaxpath())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(SuggestionsAndInnovations_PageObjects_2.tile_TextAreaxpath(), testData.getData("Title"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.linkADoc_Add_buttonxpath())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects_2.linkADoc_Add_buttonxpath())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.saveSupportingDocs_buttonxpath())) {
            error = "Failed to wait for 'Save supporting documents' button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects_2.saveSupportingDocs_buttonxpath())) {
            error = "Failed to click on 'Save supporting documents' button.";
            return false;
        }

        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.supportingDocument_xpath(testData.getData("Title")))) {
            error = "Failed to wait for uploaded Supporting documen: " + testData.getData("Title") + ".";
            return false;
        }

        return true;
    }

}
