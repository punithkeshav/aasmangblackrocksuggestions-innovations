/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.IncidentPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.SuggestionsAndInnovations_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Suggestions and Innovation Actions",
        createNewBrowserInstance = false
)

public class FR2_Capture_Suggestions_and_Innovations_Actions extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR2_Capture_Suggestions_and_Innovations_Actions()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest(){
        if (!navigateToActionsTab()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully capture the 'Suggestions And Innovations Actions'.");
    }

    public boolean navigateToActionsTab(){
        //Navigate To Actions Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.actionsTab())){
            error = "Failed to wait for 'Actions' tab";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.actionsTab())){
            error = "Failed to click on 'Actions' tab";
            return false;
        }
        
        //Actions Panel
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.actionPanel())){
            error = "Failed to wait for 'Actions' panel.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Actions' tab.");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.addButton())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.addButton())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Action Detail' window.");
        
        return true;
    }
    
    public boolean enterDetails(){
         //Process flow 2 
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.processFlow2())){
            error = "Failed to wait for 'Process Flow' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.processFlow2())){
            error = "Failed to click on 'Process Flow' tab.";
            return false;
        }
        
         //Action Detail - Action Description
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.actionDescriptionTextarea())){
            error = "Failed to wait for 'Action description' textarea.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(SuggestionsAndInnovations_PageObjects.actionDescriptionTextareaField(), getData("Action Description"))){
            error = "Failed to enter '" + getData("Action Description") + "' into 'Action description' textarea.";
            return false;
        }
        
        //Department Responsible
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.departmentResponsibleTab())){
            error = "Failed to wait for 'Department Responsible' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.departmentResponsibleTab())){
            error = "Failed to click on 'Department Responsible' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on 'Department Responsible' tab.");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.departmentResponsible(getData("Department Responsible")))){
            error = "Failed to wait for '" + getData("Department Responsible") + "' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.departmentResponsible(getData("Department Responsible")))){
            error = "Failed to click on '" + getData("Department Responsible") + "' option.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully clicked on '" + getData("Department Responsible") + "' option.");
        
        //Responsible person 
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.responsiblePersonTab())){
            error = "Failed to wait for 'Responsible Person' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.responsiblePersonTab())){
            error = "Failed to click on 'Responsible Person' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on 'Responsible Person' tab.");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.responsiblePerson(getData("Responsible Person")))){
            error = "Failed to wait for '" + getData("Responsible Person") + "' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.responsiblePerson(getData("Responsible Person")))){
            error = "Failed to click on '" + getData("Responsible Person") + "' option.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully clicked on '" + getData("Responsible Person") + "' option.");
        
        //Action due date
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.actionDueDateTab())){
            error = "Failed to wait for 'Action Due Date' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(SuggestionsAndInnovations_PageObjects.actionDueDate(), getData("Due Date"))){
            error = "Failed to enter '" + getData("Due Date") + "' into 'Action Due Date' field.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully entered '" + getData("Due Date") + "' into 'Action Due Date' field.");
        
        //Replicate mutliple users CheckBox
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.replicateChkbx())){
            error = "Failed to wait for 'Replicate mutliple users' CheckBox.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.replicateChkbx())){
            error = "Failed to click on 'Replicate mutliple users' CheckBox.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on 'Replicate mutliple users' CheckBox.");
        
        //Replicate mutliple users tab
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.multipleUsersTab())){
            error = "Failed to wait for 'Replicate mutliple users' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.selectMultipleUsers())){
            error = "Failed to click on 'Replicate mutliple users' tab.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully clicked on 'Replicate mutliple users' tab.");
        
        //Save
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.saveButton())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.saveButton())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
        //Saving mask
        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        pause(40000);
        narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
                
        return true;
    }

}
