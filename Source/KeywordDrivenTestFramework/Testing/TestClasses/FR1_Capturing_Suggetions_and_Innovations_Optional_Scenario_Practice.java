/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.SuggestionsAndInnovations_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.SuggestionsAndInnovations_PageObjects_2;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SYotsi
 */
@KeywordAnnotation(
        Keyword = "Capturing Suggestions and Innovation Optional Scenario Practice",
        createNewBrowserInstance = false
)

public class FR1_Capturing_Suggetions_and_Innovations_Optional_Scenario_Practice extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capturing_Suggetions_and_Innovations_Optional_Scenario_Practice() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!NavigateToTheSupportingDocumentsTab()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed Capturing Suggestions and Innovation - Optional Scenario.");
    }

    public boolean NavigateToTheSupportingDocumentsTab() {
        
        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.supportingDocuments_Tabxpath())) {
            error = "Failed to wait for 'Supporting Documents' tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects_2.supportingDocuments_Tabxpath())) {
            if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.supportingDocuments_Tabxpath())) {
                error = "Failed to wait for 'Supporting Documents' tab.";
                return false;
            }
            pause(5000);
            if (!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects_2.supportingDocuments_Tabxpath())) {
                error = "Failed to click on 'Supporting Documents' tab.";
                return false;
            }
        }

        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Supporting Documents' tab.");

       
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_Upload())){
            error = "Failed to wait for upload button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_Upload())){
            error = "Failed to click upload button.";
            return false;
        }
        
        String pathofImages = System.getProperty("user.dir") + "\\SikuliImages\\Images";

        String path = new File(pathofImages).getAbsolutePath();
        System.out.println("path " + pathofImages);
        
//        if(!sikuliDriverUtility.MouseClickElement(SuggestionsAndInnovations_PageObjects.SaI_Search())){
//            error = "Failed to click search field.";
//            return false;
//        }
        if(!sikuliDriverUtility.EnterText(SuggestionsAndInnovations_PageObjects.SaI_Search(), path)){
            error = "Failed to enter file name.";
            return false;
        }
        if(!SeleniumDriverInstance.selectIdentificationType()) {
                error = "Failed to click Add more images ";
                return false;
        }

        if(!sikuliDriverUtility.WaitForElementToAppear(SuggestionsAndInnovations_PageObjects.dvt2Pic())){
            error = "Failed to wait for image.";
            return false;
        }
        if(!sikuliDriverUtility.MouseMoveElement(SuggestionsAndInnovations_PageObjects.dvt2Pic())){
            error = "FAiled to move mouse to image.";
            return false;
        }
        if(!sikuliDriverUtility.MouseClickElement(SuggestionsAndInnovations_PageObjects.dvt2Pic())){
            error = "Failed ot click image.";
            return false;
        }
        if(!sikuliDriverUtility.MouseClickElement(SuggestionsAndInnovations_PageObjects.openPic())){
            error = "Failed to click open button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("SUCC");

        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.saveSupportingDocs_buttonxpath())) {
            error = "Failed to wait for 'Save supporting documents' button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects_2.saveSupportingDocs_buttonxpath())) {
            error = "Failed to click on 'Save supporting documents' button.";
            return false;
        }

        pause(5000);

//        if (!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects_2.supportingDocument_xpath(testData.getData("Title")))) {
//            error = "Failed to wait for uploaded Supporting documen: " + testData.getData("Title") + ".";
//            return false;
//        }

        return true;
    }

}
