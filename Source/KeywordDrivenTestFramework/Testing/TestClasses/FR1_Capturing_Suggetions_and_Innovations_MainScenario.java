/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.SuggestionsAndInnovations_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Suggestions and Innovation",
        createNewBrowserInstance = false
)

public class FR1_Capturing_Suggetions_and_Innovations_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR1_Capturing_Suggetions_and_Innovations_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!IsometrixNavigateSuggestionsAndInnovations())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        if(!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Completed Capture Suggestions And Innovations");
    }

    public boolean IsometrixNavigateSuggestionsAndInnovations(){
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Suggestions And Innovations
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.suggestions_and_innovations())){
            error = "Failed to wait for 'Suggestions And Innovations.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.suggestions_and_innovations())){
            error = "Failed to click on 'Suggestions And Innovations' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Suggestions And Innovations' tab.");
       
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_Add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_Add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }
    
    public boolean enterDetails(){
         //Process flow 2 
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.processFlow())){
            error = "Failed to wait for 'Process Flow' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.processFlow())){
            error = "Failed to click on 'Process Flow' tab.";
            return false;
        }
        
        //Business Unit
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_BusinessUnit())){
            error = "Failed to wait for 'Business Unit' dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_BusinessUnit())){
            error = "Failed to click Business Unit dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_BusinessUnit_Select(testData.getData("Business Unit")))){
            error = "Failed to wait for Business Unit option: '"+testData.getData("Business Unit")+"'";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_BusinessUnit_Select(testData.getData("Business Unit")))){
            error = "Failed to select Business Unit option: '"+testData.getData("Business Unit")+"'";
            return false;
        }
        narrator.stepPassedWithScreenShot("Business unit entered.");
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_ImpactType())){
            error = "Failed to wait for 'Impact Type' dropdown.";
            return false;
        }

        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_ImpactType())){
            error = "Failed to click Impact Type dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(SuggestionsAndInnovations_PageObjects.SaI_ImpactType_Select(testData.getData("Impact Type")))){
            error = "Failed to wait for Impact Type options.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_ImpactType_SelectAll())){
            error = "Failed to wait for Impact Type select all button.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_ImpactType_SelectAll())){
            error = "Failed to click Impact Type select all button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Impact type selected.");
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_Suggestion())){
            error = "Failed to wait for 'Suggestion' textarea.";
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(SuggestionsAndInnovations_PageObjects.SaI_Suggestion(), testData.getData("Suggestion"))){
            error = "Failed to enter text into 'Suggestions' textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Suggestion entered.");
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestedBy())){
            error = "Failed to wait for 'Suggested By' textarea.";
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestedBy(), testData.getData("Suggested by"))){
            error = "Failed to enter text into 'Suggested By' textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Suggested by entered.");
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionAssignedTo())){
            error = "Failed to wait for 'Suggestion Assigned to' dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionAssignedTo())){
            error = "Failed to click 'Suggestion Assigned to' dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionAssignedTo_Select(testData.getData("Assigned to")))){
            error = "Failed to wait for '"+testData.getData("Assigned to")+"' in Assigned To dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionAssignedTo_Select(testData.getData("Assigned to")))){
            error = "Failed to select '"+testData.getData("Assigned to")+"' from Assigned To dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Assigned to selected.");
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionLoggedBy())){
            error = "Failed to wait for 'Logged By' dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionLoggedBy())){
            error = "Failed to click 'Logged by' dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionLoggedBy_Select(testData.getData("Logged by")))){
            error = "Failed to wait for '"+testData.getData("Logged by")+"' in Logged by dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionLoggedBy_Select(testData.getData("Logged by")))){
            error = "Failed to click '"+testData.getData("Logged by")+"' from Logged by dropdwon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Logged by selected.");
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionType())){
            error = "Failed to wait for 'Suggestion type' dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionType())){
            error = "Failed to click 'Suggestion type' dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionType_Select(testData.getData("Suggestion type")))){
            error = "Failed to wait for '"+testData.getData("Suggestion type")+"' in Suggestion type dropwdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SuggestionType_Select(testData.getData("Suggestion type")))){
            error = "Failed to click '"+testData.getData("Suggestion type")+"' from Suggestion type dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Suggestion type selected.");
        
        narrator.stepPassedWithScreenShot("Details entered.");
                
        if(!SeleniumDriverInstance.waitForElementByXpath(SuggestionsAndInnovations_PageObjects.SaI_SaveToContinue())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(SuggestionsAndInnovations_PageObjects.SaI_SaveToContinue())){
            error = "Failed to click 'Save to continue' button.";
            return false;
        }
        
        return true;
    }

}
